:: ##############
:: # nvidia-smi #
:: ##############

@echo off

::changing working directory
cd NVSMI

echo check power draw.
nvidia-smi -i 0 --loop-ms=2500 --format=csv,noheader --query-gpu=power.draw