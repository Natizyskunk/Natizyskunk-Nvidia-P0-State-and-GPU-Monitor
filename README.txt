###########################################################################
#                                                                         #
#                   'Nvidia P0-State and GPU Monitor'                     #
#                              Version 1.0                                #
#                       created by Natizyskunk.                           #
#                              It include:                                #
#          -Nvidia P0-State and GPU Monitor v1.0 by Natizyskunk           #
#            GIT-Hub Repo: (https://github.com/Natizyskunk).              #
#                                                                         #
###########################################################################


##############################################################################################################
# See the tool in action: https://drive.google.com/file/xxXXxxXX #
##############################################################################################################


############################
#######INCLUDED-TOOLS#######
############################
# - Natizyskunk-Nvidia-P0-State-and-GPU-Monitor-v1.0
# - NVIDIA ProfileInspector-2.1.3.4
# - NVIDIA SMI
############################
#####INCLUDED-TOOLS-END#####
############################


############################
########REQUIREMENTS########
############################
# - WINDOWS x32 or x64 bits.
# - NVIDIA GPU (of course).
# - NVIDIA Drivers.
# - MSI AfterBuner (Optional).
############################
######REQUIREMENTS-END######
############################


############################
########INSTRUCTIONS########
############################
# First you need to give NVSMI some autorisations to your windows account. To do so:
# -> Go in NVSMI folder and right-click on 'nvidia-smi.exe', click 'Properties' and go in the 'Security' tab. Now here you need to add a new user to the autorisations.
# to do so just click 'Modify' and in the new window click on 'Add'. Now click on 'Advanced' and in the new windows click the 'Research' button. Now you need to find your user account that you're actually logged in.
# just double click on it to add it to the list and click on 'OK'. give now your user all autorisations and click 'Apply' and 'OK'. Click a second time on 'Apply' and now 'Exit'.
# 
# 
# 
# 
############################
######INSTRUCTIONS-END######
############################


############################
#########MORE-INFOS#########
############################
# - 
# - 
# - 
############################
#######MORE-INFOS-END#######
############################


############################
#########DONATIONS##########
############################
# You can pay me a beer with a little donation to support the porject if you want.
# ETN: etnkFBZBc2oD4UTnmKAZip1fHmR3ayUaWNwhcC7ijCawboEJMQrKXQUf639y7oCA1ZUod4zv59tvkMHcbh3rX3ut1DMy8PSEky
# XMR: 49Qxq1n8djseHNpcrHsKfvDo31Hwm5iKWHMCXuCVhp61GYSKnYGfVwbDn8NvR8a9ePbFaMUKue9x8MFX9TAgQi1oUcZQoS6 
# BTC: 19Ap5eTzST8AkmHXbdZoSJKmyztRXEy26p
# BCH: 16LgZfMJdHYUytFysE8GCo5xXjhex7Xgir
# ETH: 0xcd2af8a548569f5a3c289bafd12fd35fde6adcbd
# ETC: 0x2858deda05c6845386e8a65fe87486833e0f3706
# LTC: Li2AjPfxbNjLdo8seXdrPvQYG8JSBy6a6e
# ZEC: t1QAnpU3YVrCChNYSaU5G2QP4ZB4R6DmwXm
# DGE: DLSoZccXP4ztgQTxSUsiMdSUYU8JGZzcLv
# DSH: XfGBjovb7vG7eddpto8B5YCfRzd4qrnvDR
# EOS: 0xcd2af8a548569f5a3c289bafd12fd35fde6adcbd
# PAY: 0xcd2af8a548569f5a3c289bafd12fd35fde6adcbd
# CVC: 0xcd2af8a548569f5a3c289bafd12fd35fde6adcbd
# VRM: VP5z8w3o6CkL2ZtyHKk7B4X3scfnh9FU1R
# PIRL: 0x52c81c3D83BABe92536C2227364926aa6Ba4452c
# WAVES: 3PDd2xzTD7aG8acMVYkcaWbM1WnKWv9kqkC
############################
#######DONATIONS-END########
############################

-----------------------------------------------------------------------------------------------------------------
This document was edited by Natizyskunk on the 03 December 2017 at 6:30 PM.
-----------------------------------------------------------------------------------------------------------------
