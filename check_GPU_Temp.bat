:: ##############
:: # nvidia-smi #
:: ##############

@echo off

::changing working directory
cd NVSMI

echo check gpu temperature.
nvidia-smi -i 0 --loop-ms=2500 --format=csv,noheader --query-gpu=temperature.gpu