:: ##############
:: # nvidia-smi #
:: ##############

@echo off

:: changing working directory
cd NVSMI

echo set power limit to 90 W.
start nvidia-smi -i 0 -pl 90

echo.

echo check power draw.
start /B nvidia-smi -i 0 --loop-ms=2500 --format=csv,noheader --query-gpu=power.draw