:: ##############
:: # nvidia-smi #
:: ##############

@echo off

:: setting GPU parameters + force P0 State.
start set_GPU_force_P0-State.bat

:: setting power limit to 90 W and check GPU power draw.
start set_GPU_Power_Limit+check_Power_Draw.bat