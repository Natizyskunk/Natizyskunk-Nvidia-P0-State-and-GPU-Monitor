:: ##############
:: # nvidia-smi #
:: ##############

@echo off

:: changing working directory.
cd NVSMI

echo checking GPU Performance State.
nvidia-smi -i 0 -q -d PERFORMANCE
pause