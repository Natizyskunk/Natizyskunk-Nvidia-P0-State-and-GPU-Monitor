:: ##############
:: # nvidia-smi #
:: ##############

@echo off

:: changing working directory.
cd NVSMI

echo check all supported frequencies in the different power states that your GPU can use.
nvidia-smi -q -d SUPPORTED_CLOCKS | more
pause