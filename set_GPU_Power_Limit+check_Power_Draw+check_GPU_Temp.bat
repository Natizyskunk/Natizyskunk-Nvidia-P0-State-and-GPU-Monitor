:: ##############
:: # nvidia-smi #
:: ##############

:: setting GPU power limit to 90 W and checking GPU draw every 2500 ms.
start set_GPU_Power_Limit+check_Power_Draw.bat

:: start checking the GPU temperature every 2500 ms.
start check_GPU_Temp.bat