@echo off

:: changing working directory.
cd NVPI_2.1.3.4

:START
echo Forcing GPU P0-State.
nvidiaProfileInspector -setBaseClockOffset:0,0,220 -setMemoryClockOffset:0,0,450 -setOverVoltage:0,0 -setPowerTarget:0,100 -setTempTarget:0,0,55 -forcepstate:0,0
TIMEOUT /T 3 /NOBREAK
echo.
goto START
