:: ##############
:: # nvidia-smi #
:: ##############

@echo off

::changing working directory
cd NVSMI

echo check power limit.
nvidia-smi -i 0 --loop-ms=2500 --format=csv,noheader --query-gpu=power.limit