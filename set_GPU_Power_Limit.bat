:: ##############
:: # nvidia-smi #
:: ##############

@echo off

:: changing working directory
cd NVSMI

echo set power limit to 90 W.
start nvidia-smi -i 0 -pl 110
pause